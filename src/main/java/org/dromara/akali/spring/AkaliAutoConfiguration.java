package org.dromara.akali.spring;

import org.dromara.akali.config.AkaliProperty;
import org.dromara.akali.strategy.FallbackStrategy;
import org.dromara.akali.strategy.MethodHotspotStrategy;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(AkaliProperty.class)
public class AkaliAutoConfiguration {

    @Bean
    public AkaliScanner akaliScanner(){
        return new AkaliScanner();
    }

    @Bean
    public PropertyInit propertyInit(AkaliProperty property){
        return new PropertyInit(property);
    }

    @Bean
    public FallbackStrategy fallbackStrategy(PropertyInit propertyInit){
        return new FallbackStrategy();
    }

    @Bean
    public MethodHotspotStrategy methodHotspotStrategy(PropertyInit propertyInit){
        return new MethodHotspotStrategy();
    }


}
